// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeGame/Public/Core/InteractiveObject.h"
#include "Food.generated.h"


class ASnakeGameGameModeBase;
UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractiveObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent * StaticMeshComponent;

	UPROPERTY(BlueprintAssignable)
	FOnInteract OnInteract;

	UPROPERTY()
	ASnakeGameGameModeBase* GameMode;

public:

	UFUNCTION()
	void SetGameMode(ASnakeGameGameModeBase* SnameGameMode) {GameMode = SnameGameMode;}
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Actor) override;

	UFUNCTION( )
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent, 
					  AActor* OtherActor, 
					  UPrimitiveComponent* OtherComp, 
					  int32 OtherBodyIndex, 
					  bool bFromSweep, 
					  const FHitResult &SweepResult );
};
