// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SnakePawn.generated.h"

UENUM()
enum class EMovementDirection
{
	EMD_UP,
	EMD_DOWN,
	EMD_RIGHT,
	EMD_LEFT
};

class ASnakeElement;
UCLASS()
class SNAKEGAME_API ASnakePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASnakePawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintPure)
	float GetMovementSpeed() {return MovementSpeed;}

	UFUNCTION()
	float GetSnakeSize() {return SnakeElementSize;}

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsSetDirection;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanMove;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<ASnakeElement> SnakeElementClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<ASnakeElement*> SnakeElements;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float SnakeElementSize;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float MovementSpeed;

	UPROPERTY()
	EMovementDirection LastMovementDirection;
		
	

	UFUNCTION()
	void MoveForward(float Value);
	UFUNCTION()
	void MoveRight(float Value);
	
	UFUNCTION()
	void Move();

public:
	UFUNCTION()
	void SnakeElemOverlap(ASnakeElement* Elem, AActor *Actor);


	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int Elements = 1);
};

