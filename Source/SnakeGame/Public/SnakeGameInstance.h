// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SnakeGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API USnakeGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	virtual void Init() override;

	UFUNCTION(BlueprintSetter, BlueprintCallable)
	void SetMaxScore(int32 Score);
	UFUNCTION(BlueprintGetter, BlueprintPure)
	int32 GetMaxScore() const;

protected:
	
	UPROPERTY()
	int32 MaxScore = 0;
};
