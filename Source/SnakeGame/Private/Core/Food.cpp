// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/Food.h"

#include "Kismet/GameplayStatics.h"
#include "Pawns/SnakePawn.h"
#include "SnakeGame/SnakeGameGameModeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");
	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMeshComponent->SetCollisionResponseToChannels(ECollisionResponse::ECR_Overlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AFood::BeginOverlap);

}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Actor)
{
	IInteractiveObject::Interact(Actor);

	ASnakePawn * Snake = Cast<ASnakePawn>(Actor);

	if(Snake)
	{
		Snake->AddSnakeElement(1);
	}

	//UE_LOG(LogTemp, Warning, TEXT("Interact"));
	OnInteract.Broadcast();

	ASnakeGameGameModeBase* SnakeGameMode = Cast<ASnakeGameGameModeBase> (UGameplayStatics::GetGameMode(GetWorld()));

	if(IsValid(SnakeGameMode))
	{
		SnakeGameMode->AddScore(1);
		SnakeGameMode->FoodGenarator();
	}
	
	this->Destroy();
}

void AFood::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}
