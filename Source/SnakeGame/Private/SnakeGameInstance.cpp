// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeGameInstance.h"

void USnakeGameInstance::Init()
{
	Super::Init();
}

void USnakeGameInstance::SetMaxScore(int32 Score)
{
	if (Score > MaxScore)
	{
		MaxScore = Score;
	}
}

int32 USnakeGameInstance::GetMaxScore() const
{
	return MaxScore;
}
