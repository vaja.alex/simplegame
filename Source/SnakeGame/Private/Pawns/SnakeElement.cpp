// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawns/SnakeElement.h"
#include "Kismet/GameplayStatics.h"
#include "Pawns/SnakePawn.h"
#include "SnakeGame/SnakeGameGameModeBase.h"

// Sets default values
ASnakeElement::ASnakeElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("StaticMesh");

	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMeshComponent->SetCollisionResponseToChannels(ECollisionResponse::ECR_Overlap);
	
}

// Called when the game starts or when spawned
void ASnakeElement::BeginPlay()
{
	Super::BeginPlay();
	StaticMeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElement::BeginOverlap);
}

// Called every frame
void ASnakeElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElement::SetSnakeElemOwner(ASnakePawn* Snake)
{
	SnakeElemOwner = Snake;
	}

void ASnakeElement::DisableCollision()
{
	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ASnakeElement::Interact(AActor* Actor)
{
	IInteractiveObject::Interact(Actor);

	ASnakePawn * Snake = Cast<ASnakePawn>(Actor);

	OnInteract.Broadcast();
	
	if(Snake)
	{
		ASnakeGameGameModeBase *GameMode = Cast<ASnakeGameGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		if(GameMode)
		{
			GameMode->RestartGame();
		}
	}
}

void ASnakeElement::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	//UE_LOG(LogTemp, Warning, TEXT("BeginOverlap"));
	if(IsValid(SnakeElemOwner))
	{
		SnakeElemOwner->SnakeElemOverlap(this, OtherActor);
	}

}

