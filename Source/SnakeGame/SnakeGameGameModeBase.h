// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SnakeGameGameModeBase.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartGame);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReStartGame);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnScoreUpdate, int32, Score);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMaxScoreUpdate, int32, MaxScore);

class AFood;
UCLASS()
class SNAKEGAME_API ASnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()


public:
	
	virtual void StartPlay() override;

	UFUNCTION(BlueprintCallable)
	void StartGame();
	
	UFUNCTION(BlueprintCallable)
	void RestartGame();

	UFUNCTION(BlueprintCallable)
	void AddScore(int32 ScoreToAdd);
	
	UFUNCTION(BlueprintCallable)
	int32 GetScore() {return CurrentScore;}
	
	UFUNCTION(BlueprintSetter, BlueprintCallable)
	void SetMaxScore(int32 Score);
	
	UFUNCTION(BlueprintGetter, BlueprintPure)
	int32 GetMaxScore() const;

	UFUNCTION()
	void FoodGenarator();
	
	UPROPERTY(BlueprintReadOnly)
	float SnakeElementSize;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	TSubclassOf<AFood> Food;

	UPROPERTY(BlueprintReadWrite,EditDefaultsOnly)
	FVector2D YEdge;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
	FVector2D XEdge;
	
protected:

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	class ASnakePawn* Player;
	
	UPROPERTY()
	int32 CurrentScore = 0;
	
	UPROPERTY()
	class USnakeGameInstance* SnakeInstance;

	
	
public:
	
	UPROPERTY(BlueprintAssignable)
	FOnStartGame OnStartGame;
	UPROPERTY(BlueprintAssignable)
	FOnReStartGame OnReStartGame;
	UPROPERTY(BlueprintAssignable)
	FOnScoreUpdate OnScoreUpdate;
	UPROPERTY(BlueprintAssignable)
	FOnScoreUpdate OnMaxScoreUpdate;
};
